#!/bin/sh

arrayJPG=($(ls -d *.jpg))
arrayCR2=($(ls -d *.cr2))

index=1001

echo "Size of jpg:" ${#arrayJPG[@]}
echo "Size of cr2:" ${#arrayCR2[@]}

if [ "${#arrayJPG[@]}" -ne "${#arrayCR2[@]}" ]; then
	exit 1
fi

echo "Continue renaming"

for i in "${!arrayJPG[@]}"; do 
	printf "mv %s IMG_%d.jpg\n" "${arrayJPG[$i]}" "$index"
	mv "${arrayJPG[$i]}" IMG_"$index".jpg
	printf "mv %s IMG_%d.cr2\n" "${arrayCR2[$i]}" "$index"
	mv "${arrayCR2[$i]}" IMG_"$index".cr2
	index=$(( $index + 1 ))
done
